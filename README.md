# Unofficial Tildes Wiki

This is the _Page_ repository of the Unofficial Tildes Wiki, hosting the templates and CI to create the wiki pages.  
Visit the wiki at https://unofficial-tildes-wiki.gitlab.io/.

The archive of the previous DokuWiki hosted by Kat can be found in [the archive repo](https://gitlab.com/unofficial-tildes-wiki/tildes-wiki-archive) and browsed easily in [its GitLab wiki](https://gitlab.com/unofficial-tildes-wiki/tildes-wiki-archive/wikis/home).

### Authors
Thanks to the [original contributors](https://gitlab.com/unofficial-tildes-wiki/tildes-wiki-archive/wikis/users) of the original Dokuwiki.

See the list of [current contributors](https://gitlab.com/groups/unofficial-tildes-wiki/-/group_members) who participated in this project.

### License

Unless stated otherwise, all files in this project are licensed under the [Creative Commons Attribution 4.0 International License (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/legalcode) - see the [LICENSE.md](LICENSE.md) file for details.

## Contributing to Wiki Articles (Wiki Repository)

All of the actual Wiki article content is hosted in a GitLab wiki in a separate repository, found at https://gitlab.com/unofficial-tildes-wiki/content.  
If you want to contribute content, just follow these steps:
1. [Create a GitLab Account](https://gitlab.com/users/sign_in#register-pane) or sign in
2. [Request access to this organization](https://gitlab.com/groups/unofficial-tildes-wiki/-/group_members/request_access) - If that doesn't work, please press the "Request Access" button on [this page](https://gitlab.com/unofficial-tildes-wiki).
3. Edit the [wiki pages](https://gitlab.com/unofficial-tildes-wiki/content/wikis/pages) as you please. The formatting is powered by [Pandoc Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown), which is extremely similar to Tildes' or Reddit's formatting. Make sure to follow the content guidelines and style guide as laid out [here](https://gitlab.com/unofficial-tildes-wiki/unofficial-tildes-wiki.gitlab.io/blob/master/CONTRIBUTING.md).
4. When you're ready, request that your changes be included in UTW.gitlab.io by [creating a new issue](https://gitlab.com/unofficial-tildes-wiki/content/issues) — we'll either update UTW.gitlab.io right away or use the issue to discuss potential problems with your contribution.


## Contributing to the "Backend" (Page Repository)

You will need [Pandoc](https://pandoc.org/installing.html), `git` and [sassc](https://github.com/sass/sassc) in order to build the wiki locally. Additionally, in order to view live changes you've made, a local HTTP server is necessary (we will use Python SimpleHTTPServer as an [example below](https://gitlab.com/unofficial-tildes-wiki/unofficial-tildes-wiki.gitlab.io#setup-a-http-server)). Currently `pandoc-2.5` and `sassc-3.5.0` are used to build the wiki.

| Repo Name |  Default Directory Name (after cloning)  |                                          Link                                          | Required to Build? |                         Build Pre-reqs                          |
|-----------|----------------------------------|----------------------------------------------------------------------------------------|--------------------|-----------------------------------------------------------------|
| Page      | unofficial-tildes-wiki.gitlab.io | [Here](https://gitlab.com/unofficial-tildes-wiki/unofficial-tildes-wiki.gitlab.io.git) | YES                | `sassc` and `pandoc`                                            |
| Wiki      | content                             | [Here](https://gitlab.com/unofficial-tildes-wiki/content.git)                             | No                 | None |
| Archive   | tildes-wiki-archive              | [Here](https://gitlab.com/unofficial-tildes-wiki/tildes-wiki-archive.git)              | No                 | None                                                            |

### Installing/Building
These instructions will help you set up a local development environment from the Page repo.

For Debian/Ubuntu, adapt to your distro as needed:  
1. Fork the pages repo to allow you to contribute your changes back properly.
2. Open a terminal.
3. Install the prerequisites: `sudo apt-get install pandoc sassc git` - If a package is not available, consult the links above for download and installation instructions.
4. Clone the your fork of the Page repo, preferably via SSH: `git clone git@gitlab.com:yourname/unofficial-tildes-wiki.gitlab.io`, or, if you don't know what SSH is, via HTTPS: `git clone https://gitlab.com/yourname/unofficial-tildes-wiki.gitlab.io`
5. Go into the Page repo directory: `cd unofficial-tildes-wiki.gitlab.io`
6. Add the original repo as a remote to keep your local version up to date: `git remote add upstream git@gitlab.com:unofficial-tildes-wiki/unofficial-tildes-wiki.gitlab.io` (or via HTTPS: `git remote add upstream https://gitlab.com/unofficial-tildes-wiki/unofficial-tildes-wiki.gitlab.io`)
6. Run the build script: `./build.sh --fetch` (after that, you need `--fetch` only if you want to re-download the newest content files)
7. Create a new branch for the change you're making: `git checkout -b arbitrary-branch-name`
8. Make your changes, run the build script and check if everything looks fine, then add and commit the changes: `git add changed-file` `git commit -m "Description of what you did"`
9. Push your changes to your fork: `git push origin arbitrary-branch-name` and create a Merge Request for it on GitLab.
10. After you're finished with one feature, don't forget to switch back to the `master` branch: `git checkout master` and integrate new changes: `git pull upstream master`

After step 6, you should have deployable HTML Files in the `public/` folder. We'll now set up a server for them.  
If you have web development experience, feel free to use your preferred web server and skip this section. Otherwise, this section requires [Python](https://www.python.org/downloads/).  
__Please do not expose Python SimpleHTTPServer directly to the public internet as it only implements basic security checks.__

For Debian/Ubuntu:
1. In your terminal, move to the Page repo: `cd unofficial-tildes-wiki.gitlab.io` 
2. Then navigate to the public subfolder: `cd public`
2. Check your python version: `python -V`. If it is not installed run `sudo apt-get install python3` first.
3. If your python version is 3.X, run: `python3 -m http.server`
4. If your python version is 2.X, run: `python -m SimpleHTTPServer`
5. Open `http://127.0.0.1:8000/` in your browser to visit the local version of the wiki!

If the port `8000` is already in use, simply append a different port number e.g. `python3 -m http.server 7800` or `python -m SimpleHTTPServer 7800` and open `http://127.0.0.1:7800/` in your browser. Once you're done you may close the server process by entering `CTRL + C` in the terminal. 