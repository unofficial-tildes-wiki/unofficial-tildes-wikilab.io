# Style Guide

- Always put a blank line between headlines and paragraphs.
- Emphasize with __double underscores__ / _single underscores_ instead of asterisks whenever possible.
- The correct indentation is a single Tab character, intended to be displayed with width 2.
- To prevent pandoc errors, avoid colons (`:`) in the starting block and escape user handles in links to `\@user`.
- To prevent GitLab from changing your internal links, format them like this: `[link text](\/path/to/wiki/resource)`
- "section" headlines are created with `## Important` and sub-headlines with `### Not as important`.

# Content Guidelines

- __DO NOT__ attempt to submit content which is illegal in Germany or violates the Tildes Code of Conduct.
- __DO__ re-read everything once after you've written it and hunt for spelling mistakes.
- __DO__ attempt to use inclusive language by default, such as singular "they" for users of unknown gender.

# Optimization Recommendations

Any media contributed to the wiki should be optimized (made smaller to reduce loading times). (e.g. use `png` instead of `bmp` or for web graphics, `jpg` or `webp` instead of `raw` for large photographs)

For example you could use [optipng](http://optipng.sourceforge.net/) and [jpegoptim](https://github.com/tjko/jpegoptim) to losslessly optimize on Linux. Be sure to __make backups__ before you run anything!

The following one liners will losslessly optimize all `.png` and `.jpg`/`.jpeg` files in a folder (including subfolders):  
 - `find . -name "*.png" | while read p;do optipng -o5 --quiet "$p";done`
 - `find . -type f \( -name "*.jpg" -or -name "*.jpeg" \) | while read p;do jpegoptim -s --quiet "$p";done`