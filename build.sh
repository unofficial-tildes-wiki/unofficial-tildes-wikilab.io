#!/bin/bash

#Fetch newest content
if [[ "$1" == "--fetch" ]]; then
echo [build.sh] Fetching content from GitLab…
rm src/md -rf
git clone https://gitlab.com/unofficial-tildes-wiki/content.wiki.git src/md
rm src/md/.git -rf
#Checkout the last non-draft content commit
elif [[ "$1" == "--publish" ]]; then
echo [build.sh] Publishing content from GitLab…
rm src/md -rf
git clone https://gitlab.com/unofficial-tildes-wiki/content.wiki.git src/md
cd src/md
git checkout -q 9689948f26280348de776082508e3cbef8c1cb5e 
cd ../..
rm src/md/.git -rf
else
echo [build.sh] Skipping content fetch, force it with build.sh --fetch.
fi

#create all target directories
echo [build.sh] Creating target directories…
mkdir -p $(find src/md/ | sed "s/src\/md/public/" | sed "s/\.md//") && mkdir public/res || echo "[build.sh] Failure."

#compile css
echo [build.sh] Compiling CSS…
sassc src/sass/main.sass public/res/main.css || echo "[build.sh] Failure."

#move resources
echo [build.sh] Moving resources…
cp src/res/* public/res || echo "[build.sh] Failure."

#run all md files through pandoc
echo [build.sh] Running pandoc for slugs…
FILES=$(find src/md -type f)
for FILE in $FILES
do
  pandoc --toc --template=src/template.html $FILE -o public/$(echo $FILE | sed -r "s/src\/md\/(.+)\.md/\1/")/index.html
done

mv public/index/index.html public/index.html
rm public/index/ -rf
mv public/res/404.html public/404.html

#add favicons to root directory
echo [build.sh] Moving favicon files…
cp src/favicon/* public/ || echo "[build.sh] Failure."

#temporarily create redirects for old, non-slug links
#this portion can be removed after gitlab fixes custom 404s
echo [build.sh] Generating redirects…
<src/redirect/redirect.txt xargs -I % cp src/redirect/redirect.html %
#fix redirects to slugs instead of wiki index
while read r; do
  sed -i "0,\|gitlab.io\/|s||gitlab.io\/${r%.html}\/|" $r
  sed -i "s|gitlab.io\/public\/|gitlab.io\/|g" $r
done <src/redirect/redirect.txt
#create redirects for conslidated anime articles
mkdir -p public/anime/current public/anime/weekly
<src/redirect/redirect-anime.txt xargs -I % cp src/redirect/redirect.html %
while read anime; do
  sed -i "0,\|gitlab.io\/|s||gitlab.io\/anime\/\#recurring-topics|" $anime
done <src/redirect/redirect-anime.txt
